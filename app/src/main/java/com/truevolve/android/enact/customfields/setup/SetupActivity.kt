package com.truevolve.android.enact.customfields.setup

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.truevolve.android.enact.customfields.R
import com.truevolve.enact.Interpreter

class SetupActivity : AppCompatActivity(), SetupContract.View {
    private lateinit var presenter: SetupContract.Presenter

    override fun setPresenter(presenter: SetupContract.Presenter) {
        Log.d(TAG, "Setting presenter")
        this.presenter = presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        SetupPresenter(this@SetupActivity)

        if (intent.hasExtra(Interpreter.END)) {
            Log.d(TAG, "onCreate: ENDED")
        }
        if (intent.hasExtra(Interpreter.ERROR)) {
            Log.d(TAG, "onCreate: ERRORED")
            val throwable = intent.getSerializableExtra("exception") as Throwable
            Log.e(TAG, "onCreate: Errored: ", throwable)
        }

        presenter.startInterpreter(this@SetupActivity)

    }

    companion object {
        private val TAG = SetupActivity::class.java.simpleName
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: RESUMING")
    }
}
