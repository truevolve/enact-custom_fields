package com.truevolve.android.enact.customfields.setup

import com.truevolve.android.enact.customfields.BasePresenter
import com.truevolve.android.enact.customfields.BaseView

/**
 * Created by developer on 5/10/17.
 */

interface SetupContract {

    interface View : BaseView<Presenter> {
    }

    interface Presenter : BasePresenter {
        fun startInterpreter(activity: SetupActivity);
    }
}
