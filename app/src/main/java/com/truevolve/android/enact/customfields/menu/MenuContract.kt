package com.truevolve.android.enact.customfields.menu

import android.content.Context
import com.truevolve.android.enact.customfields.BasePresenter
import com.truevolve.android.enact.customfields.BaseView

/**
 * Created by developer on 5/10/17.
 */
interface MenuContract {

    interface View : BaseView<Presenter> {
        fun goToActivity(onStart: String)
    }

    interface Presenter : BasePresenter {
        fun handleStartButtonClick(onStart: String, activity: Context)
    }
}
