package com.truevolve.android.enact.customfields.setup

import com.truevolve.android.enact.custom_fields.custom_fields.CustomFieldsActivity
import com.truevolve.android.enact.customfields.menu.MenuActivity
import com.truevolve.android.enact.customfields.summary.SummaryActivity
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONException
import java.util.*

/**
 * Created by developer on 5/10/17.
 */

class SetupPresenter(val view: SetupContract.View) : SetupContract.Presenter {

    val TAG = "SetupPresenter"
    val policy = "{\r\n  \"start\": {\r\n    \"on_back_pressed\": \"end\",\r\n    \"on_start\": \"custom_fields\",\r\n    \"store_as\": \"event_type\",\r\n    \"type\": \"menu\"\r\n  },\r\n  \"custom_fields\": {\r\n    \"display_message\": \"Specify the following\",\r\n    \"next_button_text\": \"Done\",\r\n    \"custom_fields\": [\r\n      {\r\n        \"display_text\": \"Text input\",\r\n        \"type\": \"text\"\r\n      },\r\n      {\r\n        \"display_text\": \"Number input\",\r\n        \"type\": \"number\",\r\n        \"min\": 10,\r\n        \"max\": 55\r\n      },\r\n      {\r\n        \"display_text\": \"This is some text for a switch\",\r\n        \"type\": \"switch\"\r\n      },\r\n      {\r\n        \"display_text\": \"This is some text for a checkbox\",\r\n        \"type\": \"checkbox\"\r\n      },\r\n      {\r\n        \"display_text\": \"This is some text for a slider\",\r\n        \"type\": \"slider\",\r\n        \"min\": 10,\r\n        \"max\": 25\r\n      }\r\n    ],\r\n    \"on_next\": \"summary\",\r\n    \"store_as\": \"picker_count\",\r\n    \"type\": \"custom_fields\"\r\n  },\r\n  \"summary\": {\r\n    \"display_image\": \"thumbsUp\",\r\n    \"display_message\": \"All done! Thank you for your participation.\",\r\n    \"on_back_pressed\": \"start\",\r\n    \"on_done\": \"end\",\r\n    \"type\": \"summary\"\r\n  }\r\n}"

    override fun startInterpreter(activity: SetupActivity) {
        try {
            val interpreter = Interpreter.setup(SetupActivity::class.java, policy, setupListOfControllers())
            interpreter.start(activity)

        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: PolicyException) {
            e.printStackTrace()
        } catch (e: InterpreterException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }
    }

    fun setupListOfControllers(): ArrayList<Class<out ActivityBaseController>> {

        val listOfControllers = ArrayList<Class<out ActivityBaseController>>()
        listOfControllers.add(MenuActivity::class.java)
        listOfControllers.add(CustomFieldsActivity::class.java)
        listOfControllers.add(SummaryActivity::class.java)

        return listOfControllers
    }

    override fun stop() {

    }

    override fun start() {
    }

    init {
        view.setPresenter(this)
    }

}
