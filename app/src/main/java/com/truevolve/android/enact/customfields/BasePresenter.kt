package com.truevolve.android.enact.customfields

/**
 * Created by developer on 5/10/17.
 */
interface BasePresenter {
    fun start()
    fun stop()
}