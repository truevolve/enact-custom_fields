package com.truevolve.android.enact.customfields.summary

import android.content.Context
import com.truevolve.android.enact.customfields.BasePresenter
import com.truevolve.android.enact.customfields.BaseView

/**
 * Created by developer on 5/11/17.
 */
interface SummaryContract {

    interface View : BaseView<Presenter> {
        fun goToActivity(onDone: String)
    }

    interface Presenter : BasePresenter {
        fun handleDoneButtonClick(onDone: String, activity: Context)
    }
}