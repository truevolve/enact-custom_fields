package com.truevolve.android.enact.customfields.summary

import android.os.Bundle
import android.util.Log

import com.truevolve.android.enact.customfields.R
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.PolicyException
import kotlinx.android.synthetic.main.activity_summary.*
import org.json.JSONObject

class SummaryActivity : ActivityBaseController(), SummaryContract.View {

    private val ON_DONE = "on_done"
    private val TAG = "SummaryActivity"
    private val CONTROLLER_TYPE = "summary"

    private lateinit var presenter: SummaryContract.Presenter

    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(ON_DONE)) {
            Log.e(TAG, "validate: state object does not have $ON_DONE which is mandatory")
            throw PolicyException("State object does not have $ON_DONE which is mandatory")
        }
    }

    override val type = CONTROLLER_TYPE

    override fun setPresenter(presenter: SummaryContract.Presenter) {
        this@SummaryActivity.presenter = presenter
    }

    override fun goToActivity(onDone: String) {
        goToState(onDone)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary)

        SummaryPresenter(this@SummaryActivity)

        buttonDone.setOnClickListener {
            stateObj?.getString(ON_DONE)?.let {
                presenter.handleDoneButtonClick(it, this@SummaryActivity);
            }
        }
    }
}
