package com.truevolve.android.enact.customfields.menu

import android.content.Context
import com.truevolve.enact.Interpreter
import com.truevolve.enact.exceptions.InterpreterException
import org.json.JSONException


/**
 * Created by developer on 5/10/17.
 */
class MenuPresenter(val view: MenuContract.View) : MenuContract.Presenter {

    override fun handleStartButtonClick(onStart: String, activity: Context) {
        try {
            view.goToActivity(onStart)
        } catch (e: InterpreterException) {
            e.printStackTrace()
            Interpreter.error(activity, e)
        } catch (e: JSONException) {
            e.printStackTrace()
            Interpreter.error(activity, e)
        }
    }

    override fun start() {
    }

    override fun stop() {
    }

    init {
        view.setPresenter(this)
    }

}