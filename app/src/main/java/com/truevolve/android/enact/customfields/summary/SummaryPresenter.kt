package com.truevolve.android.enact.customfields.summary

import android.content.Context

/**
 * Created by developer on 5/11/17.
 */
class SummaryPresenter(val view: SummaryContract.View) : SummaryContract.Presenter {
    override fun start() {
    }

    override fun stop() {
    }

    override fun handleDoneButtonClick(onDone: String, activity: Context) {
        view.goToActivity(onDone)
    }

    init {
        view.setPresenter(this)
    }
}