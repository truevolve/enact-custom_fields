package com.truevolve.android.enact.customfields.menu

import android.os.Bundle
import android.util.Log
import com.truevolve.android.enact.customfields.R
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.PolicyException
import kotlinx.android.synthetic.main.activity_menu.*
import org.json.JSONObject

class MenuActivity : ActivityBaseController(), MenuContract.View {

    private val ON_START = "on_start"
    private val TAG = "MenuActivity"
    private val CONTROLLER_TYPE = "menu"

    private lateinit var presenter: MenuContract.Presenter

    override val type = CONTROLLER_TYPE

    override fun setPresenter(presenter: MenuContract.Presenter) {
        Log.d(TAG, "Setting presenter")
        this.presenter = presenter
    }

    override fun goToActivity(onStart: String) {
        goToState(onStart)
    }

    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(ON_START)) {
            Log.e(TAG, "validate: state object does not have $ON_START which is mandatory")
            throw PolicyException("State object does not have $ON_START which is mandatory")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        MenuPresenter(this@MenuActivity)

        buttonStart.setOnClickListener {
            stateObj?.getString(ON_START)?.let {
                presenter.handleStartButtonClick(it, this@MenuActivity)
            }
        }
    }
}
