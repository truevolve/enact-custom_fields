package com.truevolve.android.enact.custom_fields

/**
 * Created by developer on 5/11/17.
 */
interface BasePresenter {
    fun start()
    fun stop()
}