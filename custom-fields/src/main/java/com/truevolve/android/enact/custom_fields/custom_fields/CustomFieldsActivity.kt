package com.truevolve.android.enact.custom_fields.custom_fields

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.widget.FrameLayout
import com.truevolve.android.enact.custom_fields.R
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import kotlinx.android.synthetic.main.activity_custom_fields.*
import org.json.JSONException
import org.json.JSONObject

class CustomFieldsActivity : ActivityBaseController(), CustomFieldsContract.View {

    private val TAG = CustomFieldsActivity::class.java.simpleName
    private val DISPLAY_MESSAGE = "display_message"
    private val NEXT_BUTTON_TEXT = "next_button_text"
    private val CUSTOM_FIELDS = "custom_fields"
    private val CONTROLLER_TYPE = "custom_fields"
    private val ON_NEXT = "on_next"
    private val STORE_AS = "store_as"

    private lateinit var presenter: CustomFieldsContract.Presenter

    override val type: String = CONTROLLER_TYPE

    override fun setPresenter(presenter: CustomFieldsContract.Presenter) {
        Log.d(TAG, "Setting presenter")
        this.presenter = presenter
    }

    override fun goToActivity(onNext: String) {
        try {
            goToState(onNext)

        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: InterpreterException) {
            e.printStackTrace()
        }
    }

    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(CUSTOM_FIELDS)) {
            Log.e(TAG, "validate: state object does not have $CUSTOM_FIELDS which is mandatory")
            throw PolicyException("State object does not have $CUSTOM_FIELDS which is mandatory")
        } else if (!stateObj.has(ON_NEXT)) {
            Log.e(TAG, "validate: state object does not have $ON_NEXT which is mandatory")
            throw PolicyException("State object does not have $ON_NEXT which is mandatory")
        } else if (!stateObj.has(STORE_AS)) {
            Log.e(TAG, "validate: state object does not have $STORE_AS which is mandatory")
            throw PolicyException("State object does not have $STORE_AS which is mandatory")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_fields)

        if (stateObj?.has(DISPLAY_MESSAGE) == true) {
            try {
                displayMessage.text = stateObj?.getString(DISPLAY_MESSAGE)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        if (stateObj?.has(NEXT_BUTTON_TEXT) == true) {
            try {
                buttonDone.text = stateObj?.getString(NEXT_BUTTON_TEXT)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        try {

            stateObj?.getJSONArray(CUSTOM_FIELDS)?.let { customFields ->
                stateObj?.getString(STORE_AS)?.let { storeAs ->
                    CustomFieldsPresenter(this@CustomFieldsActivity, customFields, storeAs,
                            this@CustomFieldsActivity)

                }
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        buttonDone.setOnClickListener { presenter.saveValues() }
    }

    override fun addView(frameLayout: FrameLayout) {
        fragment_container.addView(frameLayout)
    }

    override fun saveValueSuccess(saveValueSuccess: Boolean) {

        if (saveValueSuccess) {
            stateObj?.getString(ON_NEXT)?.let {
                presenter.moveOn(it)
            }
        } else {
            val snackbar = Snackbar.make(fragment_container.rootView, getString(R.string.incomplete), Snackbar.LENGTH_LONG)
            snackbar.show()
        }
    }
}
