package com.truevolve.android.enact.custom_fields.models

/**
 * Created by developer on 5/12/17.
 */
class TypeNumber(val value: Int = 0, display_text: String) : BaseType(type = "number", display_text = display_text)