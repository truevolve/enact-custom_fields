package com.truevolve.android.enact.custom_fields.type_checkbox

import com.truevolve.android.enact.custom_fields.models.BaseType
import com.truevolve.android.enact.custom_fields.models.TypeCheckbox
import org.json.JSONObject

/**
 * Created by developer on 5/11/17.
 */
class CheckboxPresenter(val view: CheckboxFragment, val customField: JSONObject) : CheckboxContract.Presenter {

    override fun getDataModel(): BaseType {
        return TypeCheckbox(view.getValue().toBoolean(), customField.get("display_text").toString())
    }

    override fun validateValue(): Boolean {
        return true
    }

    override fun getDisplayText(): String {
        return customField.get("display_text").toString()
    }

    override fun start() {
    }

    override fun stop() {
    }

    init {
        view.setPresenter(this)
    }
}