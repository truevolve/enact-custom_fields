package com.truevolve.android.enact.custom_fields.type_slider

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.truevolve.android.enact.custom_fields.R
import kotlinx.android.synthetic.main.fragment_slider.*
import org.json.JSONObject

class SliderFragment : Fragment(), SliderContract.View {

    private lateinit var presenter: SliderContract.Presenter
    private var selectedValue: Int = 0

    companion object {
        fun newInstance(customField: JSONObject): SliderFragment {
            val fragment = SliderFragment()

            SliderPresenter(fragment, customField)

            val args = Bundle()
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getArguments() != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_slider, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        displayText.text = presenter.getDisplayText()

        selectedValue = presenter.getMinValue()
        selectedValueText.text = selectedValue.toString()

        minValue.text = presenter.getMinValue().toString()
        maxValue.text = presenter.getMaxValue().toString()

        slider_view.max = presenter.getMaxValue() - presenter.getMinValue()
        slider_view.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                selectedValue = progress + presenter.getMinValue()
                selectedValueText.text = selectedValue.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })
    }

    override fun getPresenter(): SliderContract.Presenter {
        return presenter
    }

    override fun setPresenter(presenter: SliderContract.Presenter) {
        this@SliderFragment.presenter = presenter
    }

    override fun getValue(): String {
        return selectedValue.toString()
    }
}
