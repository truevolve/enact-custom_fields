package com.truevolve.android.enact.custom_fields.models

/**
 * Created by developer on 5/12/17.
 */
class TypeSwitch(val value: Boolean = false, display_text: String) : BaseType(type = "switch", display_text = display_text)