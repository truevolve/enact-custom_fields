package com.truevolve.android.enact.custom_fields.custom_fields

import android.content.Context
import android.widget.FrameLayout
import com.truevolve.android.enact.custom_fields.BasePresenter
import com.truevolve.android.enact.custom_fields.BaseView

/**
 * Created by developer on 5/11/17.
 */
interface CustomFieldsContract {

    interface View : BaseView<Presenter> {
        fun goToActivity(onNext: String)

        fun addView(frameLayout: FrameLayout)

        fun saveValueSuccess(saveValueSuccess: Boolean)
    }

    interface Presenter : BasePresenter {
        fun handleStartButtonClick(onStart: String, activity: Context)

        fun saveValues()
        fun moveOn(onNext: String)
    }
}