package com.truevolve.android.enact.custom_fields.type_switch

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.truevolve.android.enact.custom_fields.R
import kotlinx.android.synthetic.main.fragment_switch.*
import org.json.JSONObject

class SwitchFragment : Fragment(), SwitchContract.View {

    private var presenter: SwitchContract.Presenter? = null

    companion object {
        fun newInstance(customField: JSONObject): SwitchFragment {
            val fragment = SwitchFragment()

            SwitchPresenter(fragment, customField)

            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getArguments() != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_switch, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        displayText.text = presenter?.getDisplayText()
    }

    override fun getPresenter(): SwitchContract.Presenter {
        return presenter as SwitchContract.Presenter
    }

    override fun setPresenter(presenter: SwitchContract.Presenter) {
        this@SwitchFragment.presenter = presenter
    }

    override fun getValue(): String {

        return if (switchView?.isChecked as Boolean) {
            true.toString()
        } else {
            false.toString()
        }

    }
}
