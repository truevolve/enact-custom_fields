package com.truevolve.android.enact.custom_fields.type_switch

import com.truevolve.android.enact.custom_fields.CustomFieldsBasePresenter
import com.truevolve.android.enact.custom_fields.CustomFieldsBaseView

/**
 * Created by developer on 5/11/17.
 */
interface SwitchContract {

    interface View : CustomFieldsBaseView<Presenter> {
    }

    interface Presenter : CustomFieldsBasePresenter {
        fun getDisplayText(): String;
    }
}
