package com.truevolve.android.enact.custom_fields.type_text

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.truevolve.android.enact.custom_fields.R
import kotlinx.android.synthetic.main.fragment_text.*
import org.json.JSONObject

class TextFragment : Fragment(), TextContract.View {

    private lateinit var presenter: TextContract.Presenter

    companion object {
        fun newInstance(customField: JSONObject): TextFragment {
            val fragment = TextFragment()

            TextPresenter(fragment, customField)

            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getArguments() != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_text, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        displayText.text = presenter.getDisplayText()
        displayText.visibility = View.VISIBLE
    }

    override fun getPresenter(): TextContract.Presenter {
        return presenter
    }

    override fun setPresenter(presenter: TextContract.Presenter) {
        this@TextFragment.presenter = presenter
    }

    override fun getValue(): String {
        return text_input.text.toString()
    }
}
