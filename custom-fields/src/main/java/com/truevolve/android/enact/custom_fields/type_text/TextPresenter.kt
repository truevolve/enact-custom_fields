package com.truevolve.android.enact.custom_fields.type_text

import com.truevolve.android.enact.custom_fields.models.BaseType
import com.truevolve.android.enact.custom_fields.models.TypeText
import org.json.JSONObject

/**
 * Created by developer on 5/11/17.
 */
class TextPresenter(val view: TextFragment, val customField: JSONObject) : TextContract.Presenter {

    override fun getDataModel(): BaseType {
        return TypeText(view.getValue(), customField.get("display_text").toString())
    }

    override fun validateValue(): Boolean {

        if (view.getValue().isBlank()) {
            return false
        }

        return true
    }

    override fun getDisplayText(): String {
        return customField.get("display_text").toString()
    }

    override fun start() {
    }

    override fun stop() {
    }

    init {
        view.setPresenter(this)
    }
}