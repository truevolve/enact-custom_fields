package com.truevolve.android.enact.custom_fields

/**
 * Created by developer on 5/11/17.
 */
interface CustomFieldsBaseView<T : CustomFieldsBasePresenter> : BaseView<T> {

    fun getPresenter(): T

    fun getValue(): String
}
