package com.truevolve.android.enact.custom_fields.custom_fields

import android.content.Context
import android.content.res.Configuration
import android.util.DisplayMetrics
import android.util.Log
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.truevolve.android.enact.custom_fields.CustomFieldsBasePresenter
import com.truevolve.android.enact.custom_fields.models.BaseType
import com.truevolve.android.enact.custom_fields.models.DataModel
import com.truevolve.android.enact.custom_fields.type_checkbox.CheckboxFragment
import com.truevolve.android.enact.custom_fields.type_number.NumberFragment
import com.truevolve.android.enact.custom_fields.type_slider.SliderFragment
import com.truevolve.android.enact.custom_fields.type_switch.SwitchFragment
import com.truevolve.android.enact.custom_fields.type_text.TextFragment
import com.truevolve.enact.Interpreter
import com.truevolve.enact.exceptions.ObjectNotFoundException
import org.json.JSONArray
import java.lang.reflect.InvocationTargetException


/**
 * Created by developer on 5/11/17.
 */
class CustomFieldsPresenter(val view: CustomFieldsContract.View, val customFieldsJsonArray: JSONArray, val storeAs: String, val activity: CustomFieldsActivity) : CustomFieldsContract.Presenter {
    override fun moveOn(onNext: String) {
        view.goToActivity(onNext)
    }

    private val TAG = "CustomFieldsPresenter"
    private val arrayOfPresenters = ArrayList<CustomFieldsBasePresenter>()

    override fun saveValues() {
        var shouldSave = true

        for (presenter in arrayOfPresenters) {
            if (!presenter.validateValue()) {
                Log.d(TAG, "saveValues: presenter " + presenter.javaClass.canonicalName + " is not passing validation")
                shouldSave = false
            }
        }

        if (shouldSave) {

            val mapOfCustomFields = HashMap<String, BaseType>()

            for ((i, presenter) in arrayOfPresenters.withIndex()) {
                mapOfCustomFields.put(i.toString(), presenter.getDataModel())
            }

            Interpreter.dataStore.put(storeAs, DataModel(mapOfCustomFields))
        }

        view.saveValueSuccess(shouldSave)
    }

    override fun start() {
    }

    override fun stop() {
    }

    override fun handleStartButtonClick(onStart: String, activity: Context) {
    }

    init {
        view.setPresenter(this)
        setUpFragments()
    }

    fun setUpFragments() {

        val typeString = "type"

        try {

            val fragStartID = 54321

            for (i in 0 until customFieldsJsonArray.length()) {

                val customField = customFieldsJsonArray.getJSONObject(i)

                val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                layoutParams.setMargins(30, 0, 30, calculateMargin(activity))

                val frameLayout = FrameLayout(activity)
                frameLayout.layoutParams = layoutParams

                frameLayout.id = fragStartID + i

                val type = customField.get(typeString)

                when (type) {
                    "checkbox" -> {
                        val checkboxFragment = CheckboxFragment.newInstance(customField)
                        arrayOfPresenters.add(checkboxFragment.getPresenter())
                        activity.supportFragmentManager.beginTransaction().add(frameLayout.id, checkboxFragment).commit()
                        view.addView(frameLayout)
                    }
                    "number" -> {
                        val numberFragment = NumberFragment.newInstance(customField)
                        arrayOfPresenters.add(numberFragment.getPresenter())
                        activity.supportFragmentManager.beginTransaction().add(frameLayout.id, numberFragment).commit()
                        view.addView(frameLayout)
                    }
                    "slider" -> {
                        val sliderFragment = SliderFragment.newInstance(customField)
                        arrayOfPresenters.add(sliderFragment.getPresenter())
                        activity.supportFragmentManager.beginTransaction().add(frameLayout.id, sliderFragment).commit()
                        view.addView(frameLayout)
                    }
                    "switch" -> {
                        val switchFragment = SwitchFragment.newInstance(customField)
                        arrayOfPresenters.add(switchFragment.getPresenter())
                        activity.supportFragmentManager.beginTransaction().add(frameLayout.id, switchFragment).commit()
                        view.addView(frameLayout)
                    }
                    "text" -> {
                        val textFragment = TextFragment.newInstance(customField)
                        arrayOfPresenters.add(textFragment.getPresenter())
                        activity.supportFragmentManager.beginTransaction().add(frameLayout.id, textFragment).commit()
                        view.addView(frameLayout)
                    }
                }
            }

        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: ObjectNotFoundException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }

    }


    fun calculateMargin(context: Context): Int {
        //credit to: http://stackoverflow.com/a/35006910/956975
        var value = 20
        val str: String
        if (context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            when (context.resources.displayMetrics.densityDpi) {
                DisplayMetrics.DENSITY_LOW -> {
                    str = "small-ldpi"
                    value = 20
                }
                DisplayMetrics.DENSITY_MEDIUM -> {
                    str = "small-mdpi"
                    value = 20
                }
                DisplayMetrics.DENSITY_HIGH -> {
                    str = "small-hdpi"
                    value = 20
                }
                DisplayMetrics.DENSITY_XHIGH -> {
                    str = "small-xhdpi"
                    value = 20
                }
                DisplayMetrics.DENSITY_XXHIGH -> {
                    str = "small-xxhdpi"
                    value = 20
                }
                DisplayMetrics.DENSITY_XXXHIGH -> {
                    str = "small-xxxhdpi"
                    value = 20
                }
                DisplayMetrics.DENSITY_TV -> {
                    str = "small-tvdpi"
                    value = 20
                }
                else -> {
                    str = "small-unknown"
                    value = 20
                }
            }

            Log.d(TAG, "Display is " + str)

        } else if (context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            when (context.resources.displayMetrics.densityDpi) {
                DisplayMetrics.DENSITY_LOW -> {
                    str = "normal-ldpi"
                    value = 40
                }
                DisplayMetrics.DENSITY_MEDIUM -> {
                    str = "normal-mdpi"
                    value = 40
                }
                DisplayMetrics.DENSITY_HIGH -> {
                    str = "normal-hdpi"
                    value = 40
                }
                DisplayMetrics.DENSITY_XHIGH -> {
                    str = "normal-xhdpi"
                    value = 45
                }
                DisplayMetrics.DENSITY_XXHIGH -> {
                    str = "normal-xxhdpi"
                    value = 40
                }
                DisplayMetrics.DENSITY_XXXHIGH -> {
                    str = "normal-xxxhdpi"
                    value = 50
                }
                DisplayMetrics.DENSITY_TV -> {
                    str = "normal-tvdpi"
                    value = 50
                }
                else -> {
                    str = "normal-unknown"
                    value = 40
                }
            }

            Log.d(TAG, "Display is " + str)

        } else if (context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            when (context.resources.displayMetrics.densityDpi) {
                DisplayMetrics.DENSITY_LOW -> {
                    str = "large-ldpi"
                    value = 78
                }
                DisplayMetrics.DENSITY_MEDIUM -> {
                    str = "large-mdpi"
                    value = 78
                }
                DisplayMetrics.DENSITY_HIGH -> {
                    str = "large-hdpi"
                    value = 78
                }
                DisplayMetrics.DENSITY_XHIGH -> {
                    str = "large-xhdpi"
                    value = 125
                }
                DisplayMetrics.DENSITY_XXHIGH -> {
                    str = "large-xxhdpi"
                    value = 125
                }
                DisplayMetrics.DENSITY_XXXHIGH -> {
                    str = "large-xxxhdpi"
                    value = 125
                }
                DisplayMetrics.DENSITY_TV -> {
                    str = "large-tvdpi"
                    value = 125
                }
                else -> {
                    str = "large-unknown"
                    value = 78
                }
            }

            Log.d(TAG, "Display is " + str)

        } else if (context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            when (context.resources.displayMetrics.densityDpi) {
                DisplayMetrics.DENSITY_LOW -> {
                    str = "xlarge-ldpi"
                    value = 50
                }
                DisplayMetrics.DENSITY_MEDIUM -> {
                    str = "xlarge-mdpi"
                    value = 50
                }
                DisplayMetrics.DENSITY_HIGH -> {
                    str = "xlarge-hdpi"
                    value = 50
                }
                DisplayMetrics.DENSITY_XHIGH -> {
                    str = "xlarge-xhdpi"
                    value = 50
                }
                DisplayMetrics.DENSITY_XXHIGH -> {
                    str = "xlarge-xxhdpi"
                    value = 50
                }
                DisplayMetrics.DENSITY_XXXHIGH -> {
                    str = "xlarge-xxxhdpi"
                    value = 50
                }
                DisplayMetrics.DENSITY_TV -> {
                    str = "xlarge-tvdpi"
                    value = 50
                }
                else -> {
                    str = "xlarge-unknown"
                    value = 50
                }
            }

            Log.d(TAG, "Display is $str")
        }

        return value
    }

}