package com.truevolve.android.enact.custom_fields.models

/**
 * Created by developer on 5/12/17.
 */
class TypeText(val value: String = "", display_text: String) : BaseType(type = "text", display_text = display_text)