package com.truevolve.android.enact.custom_fields.type_checkbox

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.truevolve.android.enact.custom_fields.R
import kotlinx.android.synthetic.main.fragment_checkbox.*
import org.json.JSONObject

class CheckboxFragment : Fragment(), CheckboxContract.View {

    private lateinit var presenter: CheckboxContract.Presenter

    companion object {
        fun newInstance(customField: JSONObject): CheckboxFragment {
            val fragment = CheckboxFragment()

            CheckboxPresenter(fragment, customField)

            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_checkbox, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        displayText.text = presenter.getDisplayText()
    }

    override fun getPresenter(): CheckboxContract.Presenter {
        return presenter
    }

    override fun setPresenter(presenter: CheckboxContract.Presenter) {
        this@CheckboxFragment.presenter = presenter
    }

    override fun getValue(): String {

        return if (checkBox.isChecked) {
            true.toString()
        } else {
            false.toString()
        }
    }
}
