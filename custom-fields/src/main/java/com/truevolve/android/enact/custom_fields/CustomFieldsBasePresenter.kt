package com.truevolve.android.enact.custom_fields

import com.truevolve.android.enact.custom_fields.models.BaseType

/**
 * Created by developer on 5/11/17.
 */
interface CustomFieldsBasePresenter : BasePresenter {

    fun validateValue(): Boolean
    fun getDataModel(): BaseType
}
