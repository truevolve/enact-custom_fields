package com.truevolve.android.enact.custom_fields.type_number

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.truevolve.android.enact.custom_fields.R
import kotlinx.android.synthetic.main.fragment_number.*
import org.json.JSONObject

class NumberFragment : Fragment(), NumberContract.View {

    private lateinit var presenter: NumberContract.Presenter

    companion object {
        fun newInstance(customField: JSONObject): NumberFragment {
            val fragment = NumberFragment()

            NumberPresenter(fragment, customField)

            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_number, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        displayText.visibility = View.VISIBLE
        displayText.text = presenter.getDisplayText()
    }

    override fun getPresenter(): NumberContract.Presenter {
        return presenter
    }

    override fun setPresenter(presenter: NumberContract.Presenter) {
        this@NumberFragment.presenter = presenter
    }

    override fun getValue(): String {
        return number_input.text.toString()
    }
}
