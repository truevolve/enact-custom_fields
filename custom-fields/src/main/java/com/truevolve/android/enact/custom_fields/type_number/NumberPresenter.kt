package com.truevolve.android.enact.custom_fields.type_number

import com.truevolve.android.enact.custom_fields.models.BaseType
import com.truevolve.android.enact.custom_fields.models.TypeNumber
import org.json.JSONObject

/**
 * Created by developer on 5/11/17.
 */
class NumberPresenter(val view: NumberFragment, val customField: JSONObject) : NumberContract.Presenter {

    override fun getDataModel(): BaseType {
        return TypeNumber(view.getValue().toInt(), customField.get("display_text").toString())
    }

    override fun validateValue(): Boolean {

        if (view.getValue().isBlank()) {
            return false
        }

        return true
    }

    override fun getDisplayText(): String {
        return customField.get("display_text").toString()
    }

    override fun start() {
    }

    override fun stop() {
    }

    init {
        view.setPresenter(this)
    }
}