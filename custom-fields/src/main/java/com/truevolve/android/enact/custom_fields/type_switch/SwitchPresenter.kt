package com.truevolve.android.enact.custom_fields.type_switch

import com.truevolve.android.enact.custom_fields.models.BaseType
import com.truevolve.android.enact.custom_fields.models.TypeSwitch
import org.json.JSONObject

/**
 * Created by developer on 5/11/17.
 */
class SwitchPresenter(val view: SwitchFragment, val customField: JSONObject) : SwitchContract.Presenter {

    override fun getDataModel(): BaseType {
        return TypeSwitch(view.getValue().toBoolean(), customField.get("display_text").toString())
    }

    override fun validateValue(): Boolean {
        return true
    }

    override fun getDisplayText(): String {
        return customField.get("display_text").toString()
    }

    override fun start() {
    }

    override fun stop() {
    }

    init {
        view.setPresenter(this)
    }
}