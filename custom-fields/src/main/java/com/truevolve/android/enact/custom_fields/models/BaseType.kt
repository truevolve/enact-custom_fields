package com.truevolve.android.enact.custom_fields.models

/**
 * Created by developer on 5/12/17.
 */
open class BaseType(val type: String, val display_text: String)