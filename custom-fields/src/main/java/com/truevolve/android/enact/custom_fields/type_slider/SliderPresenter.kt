package com.truevolve.android.enact.custom_fields.type_slider

import com.truevolve.android.enact.custom_fields.models.BaseType
import com.truevolve.android.enact.custom_fields.models.TypeSlider
import org.json.JSONObject

/**
 * Created by developer on 5/11/17.
 */
class SliderPresenter(val view: SliderFragment, val customField: JSONObject) : SliderContract.Presenter {

    override fun getMinValue(): Int {
        return customField.get("min") as Int
    }

    override fun getMaxValue(): Int {
        return customField.get("max") as Int
    }

    override fun getDataModel(): BaseType {
        return TypeSlider(view.getValue().toInt(), customField.get("display_text").toString())
    }

    override fun validateValue(): Boolean {
        return true
    }

    override fun getDisplayText(): String {
        return customField.get("display_text").toString()
    }

    override fun start() {
    }

    override fun stop() {
    }

    init {
        view.setPresenter(this)
    }
}